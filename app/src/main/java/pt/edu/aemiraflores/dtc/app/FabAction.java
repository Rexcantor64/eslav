package pt.edu.aemiraflores.dtc.app;

public abstract class FabAction {

    public abstract boolean run();

}

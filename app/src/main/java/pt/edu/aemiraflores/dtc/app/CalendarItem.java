package pt.edu.aemiraflores.dtc.app;

import org.json.JSONObject;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;

public class CalendarItem {

    private int type;
    private String title;
    private Calendar start;
    private Calendar end;

    public CalendarItem(JSONObject item){
        try {
            type = item.getInt("Fonte");
            title = item.getString("Titulo");
            DateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
            //System.out.println("Date: "+ dateFormat.parse(item.getString("InicioPrettyPrint")));
            (start = Calendar.getInstance()).setTime(dateFormat.parse(item.getString("InicioPrettyPrint")));
            (end = Calendar.getInstance()).setTime(dateFormat.parse(item.getString("TermoPrettyPrint")));
        }catch(Exception e){
            e.printStackTrace();
        }
    }

    public int getType() {
        return type;
    }

    public String getTitle() {
        return title;
    }

    public Calendar getStart() {
        return start;
    }

    public Calendar getEnd() {
        return end;
    }
}

package pt.edu.aemiraflores.dtc.app;

import java.util.concurrent.ExecutionException;

public abstract class AsyncRunningTask extends android.os.AsyncTask<Void, Void, Void> implements Runnable {

    @Override
    protected Void doInBackground(Void... voids){
        this.run();
        return null;
    }

    public void exec(){
        System.out.println("d");
        try {
            this.execute().get();
            System.out.println("e");
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (ExecutionException e) {
            e.printStackTrace();
        }
    }

}


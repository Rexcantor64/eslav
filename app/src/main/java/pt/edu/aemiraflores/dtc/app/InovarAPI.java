package pt.edu.aemiraflores.dtc.app;

import android.os.AsyncTask;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

public class InovarAPI extends AsyncTask<String, Void, HttpResponse> {

    public static final String BASE_URL = "https://miraflores.inovarmais.com/consulta/";
    private static final String DOMAIN = "miraflores.inovarmais.com";
    public static String xManifest = "";

    @Override
    protected HttpResponse doInBackground(String... params) {
        return run(params);
    }

    public HttpResponse run(String... params) {
        try {
            SSLUtilities.trustAllHostnames();
            SSLUtilities.trustAllHttpsCertificates();

            URL obj = new URL(BASE_URL + params[0]);
            HttpURLConnection con = (HttpURLConnection) obj.openConnection();

            // optional default is GET
            con.setRequestMethod("GET");

            // add request header
            con.setRequestProperty("Accept", "application/json, text/plain, */*");
            con.setRequestProperty("Accept-Encoding", "gzip, deflate, sdch, br");
            con.setRequestProperty("Accept-Language", "pt-PT,pt;q=0.8,en-GB;q=0.6,en;q=0.4");
            con.setRequestProperty("Authorization",
                    "Basic " + params[1]);
            con.setRequestProperty("Connection", "keep-alive");
            con.setRequestProperty("Host", DOMAIN);
            con.setRequestProperty("Referer", BASE_URL + "/app/index.html");
            con.setRequestProperty("UserAgent", "Mozilla/5.0");
            con.setRequestProperty("X-MANIFEST", xManifest);

            con.connect();

            int responseCode = con.getResponseCode();
            System.out.println("Sending 'GET' request to URL : " + params[0]);
            System.out.println("Response Code : " + responseCode);

            try {
                BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()));
                String inputLine;
                StringBuffer response = new StringBuffer();

                while ((inputLine = in.readLine()) != null) {
                    response.append(inputLine);
                }
                in.close();

                return new HttpResponse(true, responseCode, response.toString());
            } catch (FileNotFoundException e) {
                return new HttpResponse(true, responseCode, "");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return new HttpResponse(false, 0, "");
    }
}

package pt.edu.aemiraflores.dtc.app;

public class HttpResponse {

    private final boolean success;
    private final int statusCode;
    private final String page;

    public HttpResponse(boolean success, int statusCode, String page) {
        this.success = success;
        this.statusCode = statusCode;
        this.page = page;
    }

    public String getPage() {
        return page;
    }

    public boolean isSuccess() {
        return success;
    }

    public int getStatusCode() {
        return statusCode;
    }
}

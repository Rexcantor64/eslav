package pt.edu.aemiraflores.dtc.app;

import android.appwidget.AppWidgetManager;
import android.appwidget.AppWidgetProvider;
import android.content.Context;
import android.content.Intent;
import android.widget.ListView;
import android.widget.RemoteViews;
import android.widget.RemoteViewsService;

import org.json.JSONArray;

import java.io.FileInputStream;
import java.util.Arrays;

/**
 * Implementation of App Widget functionality.
 */
public class AbsentsWidget extends AppWidgetProvider {

    private JSONArray studentMissing;
    private String credentials;

    private void updateAppWidget(Context context, AppWidgetManager appWidgetManager,
                                 int appWidgetId) {

        RemoteViews views = new RemoteViews(context.getPackageName(), R.layout.absents_widget);

        ListView lv = new ListView(context);
        try {
            /*
            final LinearLayout tl = (LinearLayout) new LinearLayout(context);
            tl.removeAllViews();
            for (int i = 0; i < studentMissing.length(); i++) {
                final JSONObject obj = studentMissing.getJSONObject(i);
                final LinearLayout absentRow = (LinearLayout) View.inflate(context, R.layout.content_absents_item, null);

                final int current = i;
                //Add row to the table
                try {
                    ((TextView) absentRow.findViewById(R.id.absents_weekday)).setText(obj.getString("DiaDaSemana"));
                    ((TextView) absentRow.findViewById(R.id.absents_date)).setText(obj.getString("DataDescricao") + " " + obj.getString("Hora"));
                    ((TextView) absentRow.findViewById(R.id.absents_subject)).setText(obj.getString("Disciplina"));
                    ((TextView) absentRow.findViewById(R.id.absents_type)).setText(obj.getString("Tipo"));
                    ((TextView) absentRow.findViewById(R.id.absents_type)).setTextColor(ContextCompat.getColor(context, getAbsentColor(obj.getInt("TipoId"))));
                    ((android.support.v7.widget.AppCompatImageView) absentRow.findViewById(R.id.absents_icon)).setImageResource(getAbsentDrawable(obj.getInt("TipoId")));
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                tl.addView(absentRow);
                if (current + 1 != studentMissing.length()) {
                    View ruler = new View(context);
                    ruler.setBackgroundResource(R.color.horizontalRule);
                    tl.addView(ruler,
                            new ViewGroup.LayoutParams(ViewGroup.LayoutParams.FILL_PARENT, 2));
                }
            }*/
        } catch (Exception e) {

        }

        // Instruct the widget manager to update the widget
        appWidgetManager.updateAppWidget(appWidgetId, views);
    }

    private int getAbsentColor(int type) {
        switch (type) {
            case 1:
                return R.color.unjustifiedAbsent;
            case 2:
                return R.color.materialAbsent;
            case 3:
                return R.color.behaviourAbsent;
            case 4:
                return R.color.justifiedAbsent;
            case 5:
                return R.color.punctualityAbsent;
            case 6:
                return R.color.homeworkAbsent;
        }
        return R.color.tableHeader;
    }

    private int getAbsentDrawable(int type) {
        switch (type) {
            case 1:
                return R.drawable.ic_minus_box_outline;
            case 2:
                return R.drawable.ic_clipboard_text;
            case 3:
                return R.drawable.ic_minus_circle;
            case 4:
                return R.drawable.ic_checkbox_marked_outline;
            case 5:
                return R.drawable.ic_clock_alert;
            case 6:
                return R.drawable.ic_book_open_page_variant;
        }
        return R.drawable.ic_help;
    }

    @Override
    public void onUpdate(Context context, AppWidgetManager appWidgetManager, int[] appWidgetIds) {
        try {
            if (Arrays.asList(context.fileList()).contains("inovar_credentials")) {
                FileInputStream fis = context.openFileInput("inovar_credentials");
                StringBuilder builder = new StringBuilder();
                int ch;
                while ((ch = fis.read()) != -1) {
                    builder.append((char) ch);
                }
                fis.close();
                credentials = builder.toString();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        for (int appWidgetId : appWidgetIds) {
            updateAppWidget(context, appWidgetManager, appWidgetId);
        }
    }

    @Override
    public void onEnabled(Context context) {
        // Enter relevant functionality for when the first widget is created
    }

    @Override
    public void onDisabled(Context context) {
        // Enter relevant functionality for when the last widget is disabled
    }

    private void setupMissings() throws Exception {
        /*JSONObject currentMissings = new JSONObject(requestFromInovar("https://miraflores.inovarmais.com/consulta/api/faltas/" + studentInfo.getJSONArray("Matriculas").getJSONObject(0).getString("MatriculaId") + "/1", credentials).getPage());
        studentMissing = currentMissings.getJSONArray("Faltas");
        while (currentMissings.getString("NextRequest").length() != 0) {
            currentMissings = new JSONObject(requestFromInovar("https://miraflores.inovarmais.com/consulta/" + currentMissings.getString("NextRequest"), credentials).getPage());
            JSONArray array = currentMissings.getJSONArray("Faltas");
            for (int i = 0; i < array.length(); i++)
                studentMissing.put(array.getJSONObject(i));
        }*/
    }

    public class AbsentsItem extends RemoteViewsService {

        @Override
        public RemoteViewsFactory onGetViewFactory(Intent intent) {
            return null;
        }
    }
}


package pt.edu.aemiraflores.dtc.app;

import android.content.Intent;
import android.content.res.Configuration;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Looper;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.NavigationView;
import android.support.design.widget.Snackbar;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.GravityCompat;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Html;
import android.util.Base64;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.FileInputStream;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;

import pt.edu.aemiraflores.dtc.app.weekview.MonthLoader;
import pt.edu.aemiraflores.dtc.app.weekview.WeekView;
import pt.edu.aemiraflores.dtc.app.weekview.WeekViewEvent;

import static android.support.design.widget.Snackbar.make;
import static pt.edu.aemiraflores.dtc.app.R.id.fab;

public class MainActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener, Spinner.OnItemSelectedListener {

    public static JSONObject studentInfo;
    private JSONArray studentMissing;
    private boolean contacts = false;
    private boolean summaries = false;
    private List<CalendarItem> events = new ArrayList<CalendarItem>();
    private List<Integer> subjectsId = new ArrayList<Integer>();
    private int summariesSelected = -1;

    private HashMap<Integer, JSONObject> subjectSummaries = new HashMap<>();
    private HashMap<TextView, String> attachments = new HashMap<>();

    private FabAction fabAction;

    private String credentials;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(final View view) {
                new AsyncTask<Void, Void, Boolean>() {
                    @Override
                    protected Boolean doInBackground(Void... voids) {
                        return fabAction.run();
                    }

                    @Override
                    protected void onPostExecute(Boolean success) {
                        make(view, success ? "Dados atualizados" : "Erro ao atualizar dados!", Snackbar.LENGTH_LONG).show();
                    }

                    @Override
                    protected void onPreExecute() {
                        super.onPreExecute();
                        Snackbar.make(view, "A atualizar...", Snackbar.LENGTH_INDEFINITE).show();
                    }
                }.execute();
            }
        });
        fab.setVisibility(View.GONE);

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        try {
            FileInputStream fis = openFileInput("inovar_credentials");
            StringBuilder builder = new StringBuilder();
            int ch;
            while ((ch = fis.read()) != -1) {
                builder.append((char) ch);
            }
            credentials = builder.toString();
            fis.close();
        } catch (Exception e) {
            credentials = "";
        }

        try {
            setupStudentInfo();
        } catch (Exception e) {
            e.printStackTrace();
        }
        new AsyncTask<Void, Void, Boolean>() {
            @Override
            protected Boolean doInBackground(Void... voids) {
                try {
                    final MonthLoader.MonthChangeListener mMonthChangeListener = new MonthLoader.MonthChangeListener() {
                        @Override
                        public List<WeekViewEvent> onMonthChange(int newYear, int newMonth) {
                            // Populate the week view with some events.
                            List<WeekViewEvent> events = new ArrayList<WeekViewEvent>();
                            for (int i = 0; i < MainActivity.this.events.size(); i++) {
                                CalendarItem ci = MainActivity.this.events.get(i);
                                if (ci.getStart().get(Calendar.YEAR) == newYear && ci.getStart().get(Calendar.MONTH) + 1 == newMonth) {
                                    WeekViewEvent wve = new WeekViewEvent(i, ci.getTitle(), ci.getStart(), ci.getEnd());
                                    if (ci.getType() == 1)
                                        wve.setColor(Color.parseColor("#e2ba18"));
                                    events.add(wve);
                                }
                            }
                            return events;
                        }
                    };
                    final ViewPager pager = (ViewPager) findViewById(R.id.content_grades);
                    final WeekView mWeekView = (WeekView) findViewById(R.id.weekView);
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            mWeekView.setMonthChangeListener(mMonthChangeListener);
                            mWeekView.goToHour(8);
                            //pager.setAdapter(new GradesFragmentAdapter(getSupportFragmentManager()));
                        }
                    });
                } catch (Exception e) {
                    e.printStackTrace();
                }
                return false;
            }

            @Override
            protected void onPreExecute() {
                hideAllContents();
                showProgress(true);
            }

            @Override
            protected void onPostExecute(Boolean success) {
                showProgress(false);
                findViewById(R.id.content_main).setVisibility(View.VISIBLE);
            }
        }.execute();
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        TextView tv = (TextView) findViewById(R.id.studentName);
        TextView tv2 = (TextView) findViewById(R.id.className);
        try {
            tv.setText(studentInfo.getJSONObject("Aluno").getString("Nome"));
            tv2.setText(studentInfo.getJSONObject("Aluno").getString("Processo") + " " + studentInfo.getJSONArray("Matriculas").getJSONObject(0).getString("Turma"));
            android.support.v7.widget.AppCompatImageView iv = (android.support.v7.widget.AppCompatImageView) findViewById(R.id.imageView);
            byte[] decodedString = Base64.decode(studentInfo.getJSONObject("Aluno").getString("Avatar"), Base64.DEFAULT);
            Bitmap decodedByte = BitmapFactory.decodeByteArray(decodedString, 0, decodedString.length);
            iv.setImageBitmap(decodedByte);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            startActivity(new Intent(this, SettingsActivity.class));
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    public void onClick(View v) {
        try {
            if (v.equals(findViewById(R.id.school_address))) {
                String uri = String.format(Locale.ENGLISH, "geo:0,0?q=%s", ((TextView) v).getText());
                Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(uri));
                startActivity(intent);
            } else if (v.getId() == R.id.school_phone) {
                startActivity(new Intent(Intent.ACTION_DIAL, Uri.parse("tel:" + ((TextView) v).getText())));
            } else if (v.getId() == R.id.school_email || v.getId() == R.id.class_contact) {
                if (((TextView) v).getText().length() != 0)
                    startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("mailto:" + ((TextView) v).getText())));
            } else if (attachments.containsKey(v)) {
                JSONObject obj = new JSONObject(requestFromInovar(attachments.get(v), credentials).getPage());
                Intent i = new Intent(Intent.ACTION_VIEW);
                i.setData(Uri.parse(InovarAPI.BASE_URL + obj.getString("RequestFile")));
                startActivity(i);
            }
        } catch (Exception e) {
            e.printStackTrace();
            Snackbar.make(v, "Erro ao abrir " + ((TextView) v).getText(), Snackbar.LENGTH_SHORT).show();
        }
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();
        hideAllContents();
        showProgress(false);
        if (id == R.id.nav_missing) {
            if (studentMissing == null)
                new AsyncTask<Void, Void, Boolean>() {
                    @Override
                    protected Boolean doInBackground(Void... voids) {
                        try {
                            setupMissings();
                            return true;
                        } catch (Exception e) {
                            e.printStackTrace();
                            return false;
                        }
                    }

                    @Override
                    protected void onPreExecute() {
                        showProgress(true);
                    }

                    @Override
                    protected void onPostExecute(Boolean result) {
                        if (result) {
                            showProgress(false);
                            findViewById(R.id.content_absents).setVisibility(View.VISIBLE);
                            return;
                        }
                        Snackbar.make(
                                findViewById(R.id.content_absents), "Sem conexão!", Snackbar.LENGTH_INDEFINITE).show();
                    }
                }.execute();
            else
                findViewById(R.id.content_absents).setVisibility(View.VISIBLE);
            fabAction = new FabAction() {
                @Override
                public boolean run() {
                    try {
                        setupMissings();
                        return true;
                    } catch (Exception e) {
                        e.printStackTrace();
                        return false;
                    }
                }
            };
            findViewById(fab).setVisibility(View.VISIBLE);
        } else if (id == R.id.nav_schedule) {
            if (events.size() == 0)
                new AsyncTask<Void, Void, Boolean>() {
                    @Override
                    protected Boolean doInBackground(Void... voids) {
                        try {
                            setupCalendar();
                            return true;
                        } catch (Exception e) {
                            e.printStackTrace();
                            return false;
                        }
                    }

                    @Override
                    protected void onPreExecute() {
                        showProgress(true);
                    }

                    @Override
                    protected void onPostExecute(Boolean result) {
                        if (result) {
                            showProgress(false);
                            findViewById(R.id.content_schedule).setVisibility(View.VISIBLE);
                            return;
                        }
                        Snackbar.make(
                                findViewById(R.id.content_schedule), "Sem conexão!", Snackbar.LENGTH_INDEFINITE).show();
                    }
                }.execute();
            else
                findViewById(R.id.content_schedule).setVisibility(View.VISIBLE);
            fabAction = new FabAction() {
                @Override
                public boolean run() {
                    try {
                        setupCalendar();
                        return true;
                    } catch (Exception e) {
                        e.printStackTrace();
                        return false;
                    }
                }
            };
            findViewById(fab).setVisibility(View.VISIBLE);
        } else if (id == R.id.nav_summaries) {
            if (!summaries)
                new AsyncTask<Void, Void, Boolean>() {
                    @Override
                    protected Boolean doInBackground(Void... voids) {
                        try {
                            setupSummariesPicker();
                            return true;
                        } catch (Exception e) {
                            e.printStackTrace();
                            return false;
                        }
                    }

                    @Override
                    protected void onPreExecute() {
                        showProgress(true);
                    }

                    @Override
                    protected void onPostExecute(Boolean result) {
                        if (result) {
                            showProgress(false);
                            findViewById(R.id.content_summaries).setVisibility(View.VISIBLE);
                            return;
                        }
                        Snackbar.make(
                                findViewById(R.id.content_summaries), "Sem conexão!", Snackbar.LENGTH_INDEFINITE).show();
                    }
                }.execute();
            else
                findViewById(R.id.content_summaries).setVisibility(View.VISIBLE);
            if (summariesSelected == -1)
                findViewById(fab).setVisibility(View.GONE);
            else {
                fabAction = new FabAction() {
                    @Override
                    public boolean run() {
                        try {
                            runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    hideAllContents();
                                    showProgress(true);
                                }
                            });
                            subjectSummaries.remove(summariesSelected);
                            setupSummaries(summariesSelected);
                            runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    showProgress(false);
                                    findViewById(R.id.content_summaries).setVisibility(View.VISIBLE);
                                }
                            });
                            return true;
                        } catch (Exception e) {
                            e.printStackTrace();
                            return false;
                        }
                    }
                };
                findViewById(R.id.fab).setVisibility(View.VISIBLE);
            }
        } else if (id == R.id.nav_grades) {
            findViewById(R.id.content_grades).setVisibility(View.VISIBLE);
            findViewById(fab).setVisibility(View.GONE);
        } else if (id == R.id.nav_contacts) {
            if (!contacts)
                new AsyncTask<Void, Void, Boolean>() {
                    @Override
                    protected Boolean doInBackground(Void... voids) {
                        try {
                            setupContacts();
                            return true;
                        } catch (Exception e) {
                            e.printStackTrace();
                            return false;
                        }
                    }

                    @Override
                    protected void onPreExecute() {
                        showProgress(true);
                    }

                    @Override
                    protected void onPostExecute(Boolean result) {
                        if (result) {
                            showProgress(false);
                            findViewById(R.id.content_contacts).setVisibility(View.VISIBLE);
                            return;
                        }
                        Snackbar.make(
                                findViewById(R.id.content_contacts), "Sem conexão!", Snackbar.LENGTH_INDEFINITE).show();
                    }
                }.execute();
            else
                findViewById(R.id.content_contacts).setVisibility(View.VISIBLE);
            fabAction = new FabAction() {
                @Override
                public boolean run() {
                    try {
                        setupContacts();
                        return true;
                    } catch (Exception e) {
                        e.printStackTrace();
                        return false;
                    }
                }
            };
            findViewById(fab).setVisibility(View.VISIBLE);
        } else if (id == R.id.nav_changepsw) {

        } else if (id == R.id.nav_logout) {
            deleteFile("inovar_credentials");
            startActivity(new Intent(this, LoginActivity.class));
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    private void hideAllContents() {
        setAllContentsVisibility(View.GONE);
    }

    private void setAllContentsVisibility(int visibility) {
        try {
            findViewById(R.id.content_main).setVisibility(visibility);
            findViewById(R.id.content_absents).setVisibility(visibility);
            findViewById(R.id.content_schedule).setVisibility(visibility);
            findViewById(R.id.content_contacts).setVisibility(visibility);
            findViewById(R.id.content_summaries).setVisibility(visibility);
        } catch (NullPointerException e) {

        }
    }

    private void setupStudentInfo() throws Exception {
        final TextView tv = (TextView) findViewById(R.id.mainText);
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                try {
                    tv.setText("Bem-Vindo a Miraflores, " + studentInfo.getJSONObject("Aluno").getString("Nome"));
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });
    }

    private void setupMissings() throws Exception {
        JSONObject currentMissings = new JSONObject(requestFromInovar("api/faltas/" + studentInfo.getJSONArray("Matriculas").getJSONObject(0).getString("MatriculaId") + "/1", credentials).getPage());
        studentMissing = currentMissings.getJSONArray("Faltas");
        while (currentMissings.getString("NextRequest").length() != 0) {
            currentMissings = new JSONObject(requestFromInovar(currentMissings.getString("NextRequest"), credentials).getPage());
            JSONArray array = currentMissings.getJSONArray("Faltas");
            for (int i = 0; i < array.length(); i++)
                studentMissing.put(array.getJSONObject(i));
        }

        final LinearLayout tl = (LinearLayout) findViewById(R.id.absents);
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                tl.removeAllViews();
            }
        });
        for (int i = 0; i < studentMissing.length(); i++) {
            final JSONObject obj = studentMissing.getJSONObject(i);
            final LinearLayout absentRow = (LinearLayout) getLayoutInflater().inflate(R.layout.content_absents_item, null);

            final int current = i;
            //Add row to the table
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    try {
                        ((TextView) absentRow.findViewById(R.id.absents_weekday)).setText(obj.getString("DiaDaSemana"));
                        ((TextView) absentRow.findViewById(R.id.absents_date)).setText(obj.getString("DataDescricao") + " " + obj.getString("Hora"));
                        ((TextView) absentRow.findViewById(R.id.absents_subject)).setText(obj.getString("Disciplina"));
                        ((TextView) absentRow.findViewById(R.id.absents_type)).setText(obj.getString("Tipo"));
                        ((TextView) absentRow.findViewById(R.id.absents_type)).setTextColor(ContextCompat.getColor(MainActivity.this, getAbsentColor(obj.getInt("TipoId"))));
                        ((android.support.v7.widget.AppCompatImageView) absentRow.findViewById(R.id.absents_icon)).setImageResource(getAbsentDrawable(obj.getInt("TipoId")));
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    tl.addView(absentRow);
                    if (current + 1 != studentMissing.length()) {
                        View ruler = new View(MainActivity.this);
                        ruler.setBackgroundResource(R.color.horizontalRule);
                        tl.addView(ruler,
                                new ViewGroup.LayoutParams(ViewGroup.LayoutParams.FILL_PARENT, 2));
                    }
                }
            });
        }
    }

    private void setupCalendar() throws Exception {
        JSONArray events = new JSONArray(requestFromInovar("api/agenda/" + studentInfo.getJSONArray("Matriculas").getJSONObject(0).getString("MatriculaId") + "/1", credentials).getPage());
        this.events.clear();
        for (int i = 0; i < events.length(); i++)
            this.events.add(new CalendarItem(events.getJSONObject(i)));
    }

    private void setupContacts() throws Exception {
        final JSONObject contact = new JSONObject(requestFromInovar("api/atendimento/" + studentInfo.getJSONArray("Matriculas").getJSONObject(0).getString("TurmaId") + "/1", credentials).getPage());
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                try {
                    ((TextView) findViewById(R.id.class_headmaster)).setText(contact.getString("Nome"));
                    ((TextView) findViewById(R.id.class_attendance)).setText(contact.getString("Horario"));
                    ((TextView) findViewById(R.id.class_contact)).setText(contact.getString("Contacto"));
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });
        final JSONObject school = new JSONObject(requestFromInovar("api/entidade/escola", credentials).getPage());
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                try {
                    ((TextView) findViewById(R.id.school_name)).setText(school.getString("Nome"));
                    ((TextView) findViewById(R.id.school_address)).setText(school.getString("Morada"));
                    ((TextView) findViewById(R.id.school_phone)).setText(school.getString("Telefone"));
                    ((TextView) findViewById(R.id.school_email)).setText(school.getString("Email"));
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });
        contacts = true;
    }

    private void setupSummariesPicker() throws Exception {
        final JSONArray subjects = new JSONArray(requestFromInovar("api/aluno/matricula/disciplinas/" + studentInfo.getJSONArray("Matriculas").getJSONObject(0).getString("MatriculaId") + "/1", credentials).getPage());
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                try {
                    Spinner spinner = (Spinner) findViewById(R.id.subject_picker);
                    List<String> subjectList = new ArrayList<String>();
                    subjectList.add("Seleciona uma disciplina");
                    subjectsId.add(-1);
                    for (int i = 0; i < subjects.length(); i++) {
                        subjectsId.add(subjects.getJSONObject(i).getInt("DisciplinaId"));
                        subjectList.add(subjects.getJSONObject(i).getString("Nome"));
                    }
                    ArrayAdapter<String> adapter = new ArrayAdapter<String>(MainActivity.this,
                            android.R.layout.simple_spinner_dropdown_item, subjectList);
                    spinner.setAdapter(adapter);
                    spinner.setOnItemSelectedListener(MainActivity.this);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
        summaries = true;
    }

    private void setupSummaries(int subject) throws Exception {
        final LinearLayout ll = (LinearLayout) findViewById(R.id.summaries);
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                ll.removeAllViews();
            }
        });
        if (subject == -1)
            return;
        JSONObject summariesResult;
        if (!subjectSummaries.containsKey(subject)) {
            summariesResult =
                    new JSONObject(requestFromInovar("api/sumarios/" + studentInfo.getJSONArray("Matriculas").getJSONObject(0).getString("MatriculaId") + "/1/" + subject + "/1900-01-01", credentials).getPage());
            subjectSummaries.put(subject, summariesResult);
        } else summariesResult = subjectSummaries.get(subject);
        JSONArray summaries = summariesResult.getJSONArray("Sumarios");
        attachments.clear();
        final int summariesLenght = summaries.length();
        for (int i = 0; i < summariesLenght; i++) {
            final int ii = i;
            final JSONObject summary = summaries.getJSONObject(i);
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    try {
                        LinearLayout summaryLayout = (LinearLayout) getLayoutInflater().inflate(R.layout.content_summaries_item, null);
                        ((TextView) summaryLayout.findViewById(R.id.summary_date)).setText(summary.getString("DataPrettyPrint") + " " + summary.getString("Tempo"));
                        ((TextView) summaryLayout.findViewById(R.id.summary_lesson)).setText("Lição nº " + summary.getInt("Numero"));
                        ((TextView) summaryLayout.findViewById(R.id.summary_description)).setText(summary.getString("Descricao"));
                        LinearLayout attachments = (LinearLayout) summaryLayout.findViewById(R.id.summary_attachments);
                        for (int j = 0; j < summary.getJSONArray("Anexos").length(); j++) {
                            LinearLayout attachment = (LinearLayout) getLayoutInflater().inflate(R.layout.content_summaries_attachment, null);
                            ((TextView) attachment.findViewById(R.id.summary_attachment_name)).setText(Html.fromHtml("<u>" + summary.getJSONArray("Anexos").getJSONObject(j).getString("Nome") + "</u>"));
                            MainActivity.this.attachments.put((TextView) attachment.findViewById(R.id.summary_attachment_name), summary.getJSONArray("Anexos").getJSONObject(j).getString("RequestAnexo"));
                            attachments.addView(attachment);
                        }
                        ll.addView(summaryLayout);
                        if (ii + 1 != summariesLenght) {
                            View ruler = new View(MainActivity.this);
                            ruler.setBackgroundResource(R.color.horizontalRule);
                            ll.addView(ruler,
                                    new ViewGroup.LayoutParams(ViewGroup.LayoutParams.FILL_PARENT, 2));
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            });
        }
    }

    private HttpResponse requestFromInovar(String url, String credentials) throws Exception {
        if (Looper.myLooper() == Looper.getMainLooper())
            return new InovarAPI().execute(url, credentials).get();
        else
            return new InovarAPI().run(url, credentials);
    }

    @Override
    public void onItemSelected(AdapterView<?> adapterView, View view, final int i, long l) {
        new AsyncTask<Void, Void, Boolean>() {
            @Override
            protected Boolean doInBackground(Void... voids) {
                try {
                    final int sub = subjectsId.get(i);
                    summariesSelected = sub;
                    setupSummaries(sub);
                    if (sub == -1)
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                findViewById(fab).setVisibility(View.GONE);
                            }
                        });
                    else {
                        fabAction = new FabAction() {
                            @Override
                            public boolean run() {
                                try {
                                    runOnUiThread(new Runnable() {
                                        @Override
                                        public void run() {
                                            hideAllContents();
                                            showProgress(true);
                                        }
                                    });
                                    subjectSummaries.remove(sub);
                                    setupSummaries(sub);
                                    runOnUiThread(new Runnable() {
                                        @Override
                                        public void run() {
                                            showProgress(false);
                                            findViewById(R.id.content_summaries).setVisibility(View.VISIBLE);
                                        }
                                    });
                                    return true;
                                } catch (Exception e) {
                                    e.printStackTrace();
                                    return false;
                                }
                            }
                        };
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                findViewById(fab).setVisibility(View.VISIBLE);
                            }
                        });
                    }
                    return true;
                } catch (Exception e) {
                    e.printStackTrace();
                    return false;
                }
            }

            @Override
            protected void onPreExecute() {
                hideAllContents();
                showProgress(true);
            }

            @Override
            protected void onPostExecute(Boolean result) {
                if (result) {
                    showProgress(false);
                    findViewById(R.id.content_summaries).setVisibility(View.VISIBLE);
                    return;
                }
                Snackbar.make(
                        findViewById(R.id.content_summaries), "Sem conexão!", Snackbar.LENGTH_INDEFINITE).show();
            }
        }.execute();
    }

    @Override
    public void onNothingSelected(AdapterView<?> adapterView) {
        try {
            setupSummaries(-1);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private int getAbsentColor(int type) {
        switch (type) {
            case 1:
                return R.color.unjustifiedAbsent;
            case 2:
                return R.color.materialAbsent;
            case 3:
                return R.color.behaviourAbsent;
            case 4:
                return R.color.justifiedAbsent;
            case 5:
                return R.color.punctualityAbsent;
            case 6:
                return R.color.homeworkAbsent;
        }
        return R.color.tableHeader;
    }

    private int getAbsentDrawable(int type) {
        switch (type) {
            case 1:
                return R.drawable.ic_minus_box_outline;
            case 2:
                return R.drawable.ic_clipboard_text;
            case 3:
                return R.drawable.ic_minus_circle;
            case 4:
                return R.drawable.ic_checkbox_marked_outline;
            case 5:
                return R.drawable.ic_clock_alert;
            case 6:
                return R.drawable.ic_book_open_page_variant;
        }
        return R.drawable.ic_help;
    }

    private void showProgress(final boolean show) {
        findViewById(R.id.loading_progress).setVisibility(show ? View.VISIBLE : View.GONE);
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        WeekView mWeekView = (WeekView) findViewById(R.id.weekView);
        switch (newConfig.orientation) {
            case Configuration.ORIENTATION_LANDSCAPE:
                mWeekView.setNumberOfVisibleDays(5);
                mWeekView.goToHour(8);
                break;
            case Configuration.ORIENTATION_PORTRAIT:
                mWeekView.setNumberOfVisibleDays(3);
                mWeekView.goToHour(8);
                break;

        }
    }

}
